cupholder_height	= 75;
diameter			= 75;
radius				= diameter/2;
thickness			= .5;
support_width		= 15;
door_thickness		= 40;
module holder(){
	cylinder (h=cupholder_height,d=diameter);
}
module mount(){
	translate ([0, radius*1.5+thickness,cupholder_height]) 
		difference(){
			cube ([ support_width-.01, door_thickness+thickness*2, cupholder_height*2+thickness ],center=true);
			translate ([ 0,0,-thickness/2-.01 ])
				cube ([ support_width, door_thickness, cupholder_height*2 ], center=true);
			translate ([ 0,radius,-cupholder_height/2 ])
				cube ([ support_width+.01, door_thickness, cupholder_height*2], center=true);
		}
}
module virtual_cup(){
	translate ([0,0,thickness])cylinder (h=cupholder_height-thickness+.01,d=diameter-thickness);
}
module empty_space(){
	translate ([ support_width/2, support_width/2, -thickness ]) cube ([ radius, radius, cupholder_height-thickness*2]);
}
difference(){
	union(){
		mount();
		holder();
	}
	virtual_cup();
	empty_space();
	mirror([1,0,0]) empty_space();
	mirror([0,1,0]) empty_space();
	mirror([1,0,0]) mirror([0,1,0]) empty_space();
}
	